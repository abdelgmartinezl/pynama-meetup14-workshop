# Import of the used libraries
import requests
from bs4 import BeautifulSoup

# Job seeking
empleo = input("Empleo a Buscar: ")

# This is specific for this website
# URL forging using the value from the variable named empleo
URL = 'https://www.konzerta.com/empleos-busqueda-' + empleo + '.html'

# Let's download all the content from the forged URL
page = requests.get(URL)

# It will show the status of the page (Status 200 - OK)
#print(page.status_code)

# It will show the content of the page 
#print(page.content)

# Beautiful Soup object creation that can handle the HTML scrapped from the request
soup = BeautifulSoup(page.content, 'html.parser')

# It will show the content of the page but human readable
#print(soup.prettify())

# It will show all the tags nested on the structure of the page
#print(list(soup.children))

print("\n\nResultados - " + empleo.capitalize())

# Find Elements by Class
trabajos = soup.find_all('h2', class_='titulo-aviso')
print("\nSe han encontrado " + str(len(trabajos)) + " oportunidades.")

print("\nLas siguientes empresas buscan profesionales como tu:")
empresas = soup.find_all('h3', class_='nombre')
for empresa in empresas:
    empresa = empresa.get_text().strip().lower()
    if empresa != "confidencial":
        print(" - " + empresa.capitalize())
